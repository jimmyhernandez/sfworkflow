var fs = require('fs');
var archiver = require('archiver');

var output = fs.createWriteStream('src/staticresources/sfappresources.resource');
var archive = archiver('zip');

output.on('close',function(){
  console.log(archive.pointer() + ' total bytes');
  console.log('archiver has been finalized and the output file descriptor has closed.');
});

archive.on('error', function(err) {
  console.error(err);
});

archive.pipe(output);

// append files from a directory 
archive.directory('dist/libs/');
archive.directory('dist/styles/');
archive.directory('src/scripts/');
// finalize the archive (ie we are done appending files but streams have to finish yet) 
archive.finalize();