var compressor = require('node-minify');

compressor.minify({
    compressor: 'uglifyjs',
    input: [
        'node_modules/angular/angular.js',
        'node_modules/angular-route/angular-route.js',
        'node_modules/angular-drag-and-drop-lists/angular-drag-and-drop-lists.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/angular-ui-router/release/angular-ui-router.js',
        ],
    output: 'dist/libs/vendors.js',
    callback: function(err,result){
        if(err){
            console.log(err);
        }else{
            console.log('Success!!!');
        }
    }
});