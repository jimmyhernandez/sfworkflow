angular.module("sfwf",["ui.router","dndLists"]);

angular.module('sfwf').config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/list");

    $stateProvider.state('list',{
        url: '/list',
        templateUrl: 'apex/list'
    })
});